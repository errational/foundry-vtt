/**
 * @author Evan Clarke <errational>
 * @version 0.1
 */
var roundChanged = false;

class RerollInitiative {
    constructor(){
        this.preUpdateHook();
        this.postUpdateHook();
    }

    preUpdateHook() {
        Hooks.on("preUpdateCombat", function(combat){
            //console.log("preupdate:\n",combat);
            //detect if the pre-update  contains a value for round
            if(combat.round){
                //console.log("pre round changed");
                roundChanged = true;
                //console.log(roundChanged);
            }  
        });
    }  

    postUpdateHook() {
        Hooks.on("updateCombat", function(combat){
            console.log("postupdate:\n",combat);
            console.log(roundChanged);
            if(roundChanged){
                console.log("post round changed");
                combat.resetAll();
                combat.rollAll(combat.data.combatants);
                roundChanged = false;
                console.log(roundChanged);
            }
        }); 
    }
}

let rri = new RerollInitiative();




